#include <iostream>
#include <fstream>
#include "mylib.h"

#define CHECK_RANGE(number_to_check, range) ((number_to_check >= 0 && number_to_check < range))

#define ARRAY_SIZE 5

using namespace Lesson7;

struct Relation {
  int* self = nullptr;
  Relation* leftRelation = nullptr;
  Relation* rightRelation = nullptr;
};

#pragma pack(push, 1)
struct Employee {
  long id;
  unsigned short age;
  double salary;
};
#pragma pack(pop)

/**
 * Print separator between tasks.
 */
void PrintSeparator() {
  const std::string TASK_SEPARATOR = "========================================================";
  std::cout << TASK_SEPARATOR << std::endl;
}

int* RelationToArray(Relation* startRelation) {
  auto i_sorted_array = new int[ARRAY_SIZE];
  auto selectedRelation = startRelation;
  int i = 0;
  do {
    i_sorted_array[i] = *selectedRelation->self;
    selectedRelation = selectedRelation->rightRelation;
    i++;
  } while (selectedRelation->rightRelation != nullptr);
  i_sorted_array[i] = *selectedRelation->self;
  return i_sorted_array;
}

void AttachRelationToEnd(Relation* selectedRelation, int* i_array_to_sort) {
  auto newRelation = new Relation;
  newRelation->self = i_array_to_sort;
  newRelation->leftRelation = selectedRelation;
  selectedRelation->rightRelation = newRelation;
}

void InsertRelationBetween(Relation* startRelation, Relation* selectedRelation, int* i_array_to_sort) {
  auto leftRelation = selectedRelation->leftRelation;
  auto newRelation = new Relation;
  newRelation->self = i_array_to_sort;
  newRelation->rightRelation = selectedRelation;
  newRelation->leftRelation = leftRelation;
  selectedRelation->leftRelation = newRelation;
  if (leftRelation != nullptr) {
    leftRelation->rightRelation = newRelation;
  }
}

Relation* ArrayToSortRelation(Relation* startRelation, int* i_array_to_sort) {
  for (int i = 1; i < ARRAY_SIZE; i++) {
    bool attached = false;
    auto selectedRelation = startRelation;
    while (*selectedRelation->self < i_array_to_sort[i] && !attached) {
      if (selectedRelation->rightRelation != nullptr) {
        selectedRelation = selectedRelation->rightRelation;
      }
      else {
        AttachRelationToEnd(selectedRelation, &i_array_to_sort[i]);
        attached = true;
      }
    }
    if (!attached) {
      InsertRelationBetween(startRelation, selectedRelation, &i_array_to_sort[i]);
      if (startRelation == selectedRelation) {
        startRelation = selectedRelation->leftRelation;
      }
    }
  }
  return startRelation;
}

void PrintEmployee(Employee* employee) {
  std::cout << "Employee structure:" << std::endl;
  std::cout << "id:" << employee->id << std::endl;
  std::cout << "age:" << employee->age << std::endl;
  std::cout << "salary:" << employee->salary << std::endl;
  std::cout << "size of structure:" << sizeof(*employee) << std::endl;
}

void SaveToFile(Employee* employee) {
  std::ofstream fout("emp.txt", std::ios_base::binary);
  if (fout.is_open()) {
    fout.write((char *) employee, sizeof(*employee));
    fout.close();
  }
}

void Task1_5() {
  const size_t SIZE = 15;
  size_t positive_count = 0;
  size_t negative_count = 0;
  auto f_rand_arr = InitArray(SIZE);
  PrintArray(f_rand_arr, SIZE);
  CountPositiveArray(f_rand_arr, SIZE, &positive_count, &negative_count);
  std::cout << "Count of positive numbers:" << positive_count << std::endl;
  std::cout << "Count of negative numbers:" << negative_count << std::endl;
  delete[] f_rand_arr;
}

void Task2() {
  int i_number;
  const int I_RANGE = 27;
  std::cout << std::boolalpha;
  std::cout << "Input number to check" << std::endl;
  std::cin >> i_number;
  bool b_check_range = CHECK_RANGE(i_number, I_RANGE);
  std::cout << "Check result:" << b_check_range << std::endl;
}

void Task3() {
  auto i_array_to_sort = new int[ARRAY_SIZE];
  for (int i = 0; i < ARRAY_SIZE; i++) {
    std::cout << "Input " << i << " number of array" << std::endl;
    std::cin >> i_array_to_sort[i];
  }
  auto startRelation = new Relation;
  startRelation->self = &i_array_to_sort[0];
  startRelation = ArrayToSortRelation(startRelation, i_array_to_sort);

  auto i_sorted_array = RelationToArray(startRelation);
  PrintArray(i_array_to_sort, ARRAY_SIZE);
  PrintArray(i_sorted_array, ARRAY_SIZE);
}

void Task4() {
  auto e1 = new Employee;
  e1->id = 125037;
  e1->age = 31;
  e1->salary = 120'000.0;
  PrintEmployee(e1);
  SaveToFile(e1);
  delete e1;
}

int main() {
  srand(time(nullptr));

  {
    /**
     * Task 1, 5
     * Создайте проект из 2х cpp файлов и заголовочного (main.cpp, mylib.cpp, mylib.h)
     * во втором модуле mylib объявить 3 функции:
     * для инициализации массива (типа float), печати его на экран
     * и подсчета количества отрицательных и положительных элементов.
     * Вызывайте эти 3-и функции из main для работы с массивом.
     */
     Task1_5();
  }
  PrintSeparator();

  {
    /**
     * Task 2
     * Описать макрокоманду (через директиву define), проверяющую,
     * входит ли переданное ей число (введенное с клавиатуры) в диапазон от нуля (включительно)
     * до переданного ей второго аргумента (исключительно)
     * и возвращает true или false, вывести на экран «true» или «false».
     */
     Task2();
  }
  PrintSeparator();

  {
    /**
     * Task 3
     * Задайте массив типа int. Пусть его размер задается через директиву препроцессора #define.
     * Инициализируйте его через ввод с клавиатуры. Напишите для него свою функцию сортировки (например Пузырьком).
     * Реализуйте перестановку элементов как макрокоманду SwapINT(a, b). Вызывайте ее из цикла сортировки.
     */
     Task3();
  }
  PrintSeparator();

  {
    /**
     * Task 4
     * Объявить структуру Сотрудник с различными полями. Сделайте для нее побайтовое выравнивание
     * с помощью директивы pragma pack. Выделите динамически переменную этого типа. Инициализируйте ее.
     * Выведите ее на экран и ее размер с помощью sizeof. Сохраните эту структуру в текстовый файл.
     */
     Task4();
  }

  return 0;
}
