//
// Created by aard on 23.09.2021.
//
#include <iostream>

namespace Lesson7 {

const size_t STEP = 3;

void CountPositiveArray(const float ARRAY[], const size_t SIZE, size_t *positive_count, size_t *negative_count) {
  std::cout << "Array: " << std::endl;
  for (size_t i = 0; i < SIZE; i++) {
    if (ARRAY[i] > 0) {
      (*positive_count)++;
    } else {
      (*negative_count)++;
    }
  }
}

bool PrintArray(const float ARRAY[], const size_t SIZE) {
  if (ARRAY == nullptr || SIZE == 0) {
    return false;
  }
  std::cout << "Array: " << std::endl;
  for (size_t i = 0; i < SIZE; i++) {
    std::cout << "Index:" << i << " Value:" << ARRAY[i] << std::endl;
  }
  std::cout << std::endl;
  return true;
}

bool PrintArray(const int ARRAY[], const size_t SIZE) {
  if (ARRAY == nullptr || SIZE == 0) {
    return false;
  }
  std::cout << "Array: " << std::endl;
  for (size_t i = 0; i < SIZE; i++) {
    std::cout << "Index:" << i << " Value:" << ARRAY[i] << std::endl;
  }
  std::cout << std::endl;
  return true;
}

float *InitArray(const size_t SIZE) {
  auto *f_d_arr = new float[SIZE];
  for (size_t i = 0; i < SIZE; i++) {
    bool b_is_positive = rand() % 2;
    f_d_arr[i] = (rand() % 100 + (float(rand() % 100) / 100)) * float((b_is_positive ?: -1));
  }
  return f_d_arr;
}

}
