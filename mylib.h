//
// Created by aard on 23.09.2021.
//

#ifndef LESSON_7__MYLIB_H_
#define LESSON_7__MYLIB_H_

#include <iostream>

namespace Lesson7 {

void CountPositiveArray(const float ARRAY[], const size_t SIZE, size_t *positive_count, size_t *negative_count);

bool PrintArray(const float ARRAY[], const size_t SIZE);

float *InitArray(const size_t SIZE);

bool PrintArray(const int ARRAY[], const size_t SIZE);

}

#endif //LESSON_7__MYLIB_H_
